#include <iostream>
#include <vector>
#include <Windows.h>
#include <conio.h>
#include <time.h>
#include <random>
#include <string>
using namespace std;

void gotoxy(short x, short y)
{
	COORD pos = { x, y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
}

void DrawMap()
{
	for (int i = 0; i < 30; i++)
	{
		for (int j = 0; j < 70; j++)
		{
			if (i == 0 || i == 29)
			{
				gotoxy(j, i);
				cout << "*";
			}
			else
			{
				if (j == 0 || j == 69)
				{
					gotoxy(j, i);
					cout << "|";
				}
				else
				{
					gotoxy(j, i);
					cout << " ";
				}
			}
		}
		cout << endl;
	}
}

void PrintSnake(vector<vector<int> >&Snake)
{
	for (int i = 0; i < Snake[0].size(); i++)
	{
		gotoxy(Snake[0][i], Snake[1][i]);
		cout << "o";
	}
}

bool IsFinish(vector<vector<int> >&Snake)
{
	bool isFinish = false;
	if (Snake[0][0] > 0 && Snake[0][0] < 69 && Snake[1][0] == 0 || Snake[1][0] == 29 && Snake[0][0] < 69 && Snake[0][0]>0)
		isFinish = true;
	if (Snake[0][0] == 0 && Snake[1][0] > 0 && Snake[1][0] < 29 || Snake[0][0] == 69 && Snake[1][0]>0 && Snake[1][0] < 29)
		isFinish = true;
	return isFinish;
}

bool IsEat(vector<vector<int> >&Snake, int fruitX, int fruitY)
{
	bool isEat = false;
	if (Snake[0][0] == fruitX&&Snake[1][0] == fruitY)
		isEat = true;
	return isEat;
}

void PrintFruit(int fruitX, int fruitY)
{
	gotoxy(fruitX, fruitY);
	cout << "0";
}

int main()
{
	srand(time(NULL));
	vector<vector<int> >Snake(2);
	for (int i = 0; i < Snake.size(); i++)
		Snake[i].resize(3);
	int fruitX = 20;
	int fruitY = 10;
	Snake[0][0] = 1;
	Snake[1][0] = 10;
	Snake[0][1] = 2;
	Snake[1][1] = 10;
	Snake[0][2] = 3;
	Snake[1][2] = 10;
	char Nums = 0;
	int Score = 0;
	int trend = 3;
	_flushall();
	int oldFruitX = fruitX;
	int oldFruitY = fruitY;
	while (1)
	{
		Sleep(50);
		DrawMap();
		PrintFruit(fruitX, fruitY);
		if (_kbhit())
		{
			Nums = _getch();
			if (Nums == 97 || Nums == 61)
				trend = 2;
			if (Nums == 100 || Nums == 64)
				trend = 3;
			if (Nums == 119 || Nums == 77)
				trend = 1;
			if (Nums == 73 || Nums == 115)
				trend = 0;
		}
		for (int i = Snake[0].size() - 1; i >= 1; i--)
		{
			Snake[0][i] = Snake[0][i - 1];
			Snake[1][i] = Snake[1][i - 1];
		}
		switch (trend)
		{
		case 0:Snake[1][0] = Snake[1][0] + 1; break;
		case 1:Snake[1][0] = Snake[1][0] - 1; break;
		case 2:Snake[0][0] = Snake[0][0] - 1; break;
		case 3:Snake[0][0] = Snake[0][0] + 1; break;
		default:
			break;
		}
		if (IsFinish(Snake))
		{
			string IsOver;
			gotoxy(0, 60);
			cout << "Ban co muon choi lai khong" << endl;
			cout << "Neu co xin hay go 'Yes'" << endl;
			cout << "Neu khong xin hay go 'No'" << endl;
			cin >> IsOver;
			if (IsOver == "No")
			{
				gotoxy(0, 63);
				cout << "LOSE" << endl;
				break;
			}
			else
			{
				int fruitX = 20;
				int fruitY = 10;
				Snake[0][0] = 1;
				Snake[1][0] = 10;
				Snake[0][1] = 2;
				Snake[1][1] = 10;
				Snake[0][2] = 3;
				Snake[1][2] = 10;
				Score = 0;
				trend = 3;
				Snake.size() = 3;
			}
		}
		if (IsEat(Snake, fruitX, fruitY))
		{
			Snake[0].push_back(fruitX);
			Snake[1].push_back(fruitY);
			Score++;
			fruitX = 1 + rand() % 48;
			fruitY = 1 + rand() % 18;
			while (fruitX == oldFruitX || fruitX == oldFruitY)
			{
				fruitX = 1 + rand() % 48;
				fruitY = 1 + rand() % 18;
			}
		}
		PrintSnake(Snake);
		_flushall();
		gotoxy(70, 19);
		cout << Score;
	}
	for (int i = 0; i < Snake.size(); i++)
		Snake[i].clear();
	Snake.clear();
	gotoxy(0, 21);
	system("pause");
	return 0;
}
